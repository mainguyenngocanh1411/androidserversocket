package com.na.communicateserver;

import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;

public class Server {
    private MainActivity activity;
    private ServerSocket serverSocket;
    private String message = "";

    private static final int MAX_CLIENT = 2;
    private static final SocketServerReplyThread [] threads = new SocketServerReplyThread [MAX_CLIENT];
    private static final int socketServerPORT = 8080;

    public Server(MainActivity activity) {
        this.activity = activity;
        Thread socketServerThread = new Thread(new SocketServerThread());
        socketServerThread.start();
    }

    public int getPort() {
        return socketServerPORT;
    }

    public void onDestroy() {
        if (serverSocket != null) {
            try {
                System.out.println("Socket close");
                serverSocket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private class SocketServerThread extends Thread {

        int count = 0;

        @Override
        public void run() {
            try {
                // create ServerSocket using specified port
                serverSocket = new ServerSocket(socketServerPORT);

                while (true) {
                    // block the call until connection is created and return
                    // Socket object
                    Socket socket = serverSocket.accept();
                    count++;
                    message += "#" + count + " from "
                            + socket.getInetAddress() + ":"
                            + socket.getPort() + "\n";
                    for (int i = 0; i < MAX_CLIENT; i++) {
                        if (threads[i] == null) {
                            (threads[i] = new SocketServerReplyThread(socket, i)).start();
                            break;
                        }
                    }


//                    SocketServerReplyThread socketServerReplyThread =
//                            new SocketServerReplyThread(socket, count);
//                    socketServerReplyThread.run();

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private class SocketServerReplyThread extends Thread {

        private Socket hostThreadSocket;
        int cnt;

        SocketServerReplyThread(Socket socket, int c) {
            hostThreadSocket = socket;
            cnt = c;
        }
        public Socket getSocket(){
            return this.hostThreadSocket;
        }

        @Override
        public void run() {
            OutputStream outputStream;

            System.out.println("Thread " + threads[cnt]);
            try {

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
                        1024);
                byte[] buffer = new byte[1024];
                int bytesRead;
                String response="";
                InputStream inputStream = hostThreadSocket.getInputStream();
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    byteArrayOutputStream.write(buffer, 0, bytesRead);
                    response = byteArrayOutputStream.toString("UTF-8");
                    //split response
                    final String[] parts = response.split("\n");
                    final int length = parts.length;

                    String lastestMessage = parts[length-1];

                    //Check if new user joins the room
                    if(lastestMessage.split(":")[0].equals("NN")){
                       message= lastestMessage.split(":")[1] + " has joined this conversation\n";
                        activity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                activity.msg.setText(message);
                            }
                        });

                        for(int i=0; i<MAX_CLIENT;i++){
                            if(threads[i]!=null){
                                Socket socket = threads[i].getSocket();

                                outputStream = socket.getOutputStream();
                                PrintStream printStream = new PrintStream(outputStream);
                                printStream.print("NN:"+message+"\n");

                            }
                        }
                        //printStream.close();

                        message += "replayed: " + message + "\n";

                    }
                    else{
                        String split = lastestMessage.split(":")[0];
                        System.out.println(split);
                        String[] data = split.split("-");

                        String nickname = data[0];
                        String message = data[1];
                        for(int i=0; i<MAX_CLIENT;i++){
                            if(threads[i]!=null){

                                Socket socket = threads[i].getSocket();

                                outputStream = socket.getOutputStream();
                                PrintStream printStream = new PrintStream(outputStream);
                                printStream.print(nickname+"-"+message+":"+ lastestMessage.split(":")[1]+"\n");
                                System.out.println(nickname+"-"+message+":"+ lastestMessage.split(":")[1]);
                            }
                        }
                    }


                }




            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                message += "Something wrong! " + e.toString() + "\n";
            }

            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    activity.msg.setText(message);
                }
            });
        }
    }

    public String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress
                            .nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += "Server running at : "
                                + inetAddress.getHostAddress();
                    }
                }
            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }
        return ip;
    }
}